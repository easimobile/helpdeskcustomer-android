package com.easipos.helpdesk.models

data class ServiceType(
    val serviceTypeCode: String,
    val serviceTypeDesc: String
) {

    override fun toString(): String {
        return serviceTypeDesc
    }
}