package com.easipos.helpdesk.models

data class CaseType(
    val caseTypeCode: String,
    val caseTypeDesc: String,
    val hasCaseSubType: Boolean
) {

    override fun toString(): String {
        return caseTypeDesc
    }
}