package com.easipos.helpdesk.models

data class CustomerInfo(
    val customerName: String,
    val projectName: String,
    val contactPerson: String,
    val contactNo: String
)