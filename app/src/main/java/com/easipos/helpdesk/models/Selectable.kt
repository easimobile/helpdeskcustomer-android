package com.easipos.helpdesk.models

import java.io.Serializable

data class Selectable<T>(
    var data: T,
    var isSelected: Boolean = false
) : Serializable