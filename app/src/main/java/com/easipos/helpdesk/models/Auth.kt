package com.easipos.helpdesk.models

data class Auth(val token: String)