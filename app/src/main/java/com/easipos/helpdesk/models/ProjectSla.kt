package com.easipos.helpdesk.models

data class ProjectSla(
    val slaCode: String,
    val priority: Int,
    val responseTime: String,
    val resolutionTime: String
) {

    companion object {
        fun toString(responseTime: String, resolutionTime: String): String {
            return "Response Time: $responseTime\nResolution Time: $resolutionTime"
        }
    }

    override fun toString(): String {
        return Companion.toString(responseTime, resolutionTime)
    }
}