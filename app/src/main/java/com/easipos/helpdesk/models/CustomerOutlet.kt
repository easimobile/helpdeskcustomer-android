package com.easipos.helpdesk.models

data class CustomerOutlet(
    val outletCode: String,
    val outletName: String,
    val address1: String,
    val address2: String,
    val address3: String,
    val address4: String,
    val address5: String,
    val postalCode: String,
    val posCount: Int
) {

    companion object {
        fun toString(outletCode: String, outletName: String, address1: String,
                     address2: String, address3: String, address4: String,
                     address5: String, postalCode: String): String {
            val addresses = arrayListOf<String>()
            if (address1.isNotBlank()) {
                addresses.add(address1)
            }
            if (address2.isNotBlank()) {
                addresses.add(address2)
            }
            if (address3.isNotBlank()) {
                addresses.add(address3)
            }
            if (address4.isNotBlank()) {
                addresses.add(address4)
            }
            if (address5.isNotBlank()) {
                addresses.add(address5)
            }
            if (postalCode.isNotBlank()) {
                addresses.add(postalCode)
            }
            return when {
                addresses.isNotEmpty() -> {
                    if (outletName.isNotBlank()) {
                        "$outletCode - $outletName\n${addresses.joinToString(", ")}"
                    } else {
                        "$outletCode\n${addresses.joinToString(", ")}"
                    }
                }
                outletName.isNotBlank() -> {
                    "$outletCode - $outletName"
                }
                else -> {
                    outletCode
                }
            }
        }
    }

    override fun toString(): String {
        return Companion.toString(outletCode, outletName, address1, address2, address3,
            address4, address5, postalCode)
    }
}