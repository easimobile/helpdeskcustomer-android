package com.easipos.helpdesk.models

data class CaseSubType(
    val caseSubTypeCode: String,
    val caseSubTypeDesc: String,
    val hasCaseSubType: Boolean
) {

    override fun toString(): String {
        return caseSubTypeDesc
    }
}

data class CaseSubTypeLayering(
    val caseSubType: CaseSubType,
    val level: Int
) {

    override fun toString(): String {
        return caseSubType.toString()
    }
}