package com.easipos.helpdesk.adapters

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.easipos.helpdesk.R
import com.easipos.helpdesk.databinding.ViewCaseBinding
import com.easipos.helpdesk.models.Case
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.setSizeTextTypeToNone
import io.github.anderscheow.library.recyclerView.adapters.paging.BasePagedListAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_case.*

class CaseAdapter(private val listener: OnGestureDetectedListener,
                  callback: () -> Unit)
    : BasePagedListAdapter<Case>(Case.DIFF_CALLBACK, callback) {

    interface OnGestureDetectedListener {
        fun onSelectItem(item: Case)

        fun onRateCase(item: Case)
    }

    public override fun getBodyLayout(position: Int): Int {
        return R.layout.view_case
    }

    override fun getBodyViewHolder(viewType: Int, binding: ViewDataBinding): RecyclerView.ViewHolder {
        return CaseViewHolder(binding as ViewCaseBinding)
    }

    inner class CaseViewHolder(private val binding: ViewCaseBinding)
        : BaseViewHolder<Case>(binding), LayoutContainer {

        override val containerView: View
            get() = binding.root

        override fun extraBinding(item: Case) {
            text_view_status.setSizeTextTypeToNone()

            button_rate.click {
                listener.onRateCase(item)
            }
        }

        override fun onClick(view: View, item: Case?) {
            item?.let {
                listener.onSelectItem(item)
            }
        }
    }
}