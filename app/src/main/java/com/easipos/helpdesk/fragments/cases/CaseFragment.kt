package com.easipos.helpdesk.fragments.cases

import android.content.Context
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.main.MainActivity
import com.easipos.helpdesk.adapters.CaseAdapter
import com.easipos.helpdesk.adapters.SelectionAdapter
import com.easipos.helpdesk.api.requests.cases.GetCasesRequestModel
import com.easipos.helpdesk.api.requests.cases.GetCustomerOutletsRequestModel
import com.easipos.helpdesk.base.CustomLifecycleFragment
import com.easipos.helpdesk.constant.CaseStatus
import com.easipos.helpdesk.event_bus.RefreshCaseListing
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.fragments.cases.mvp.CasePresenter
import com.easipos.helpdesk.fragments.cases.mvp.CaseView
import com.easipos.helpdesk.fragments.cases.navigation.CaseNavigation
import com.easipos.helpdesk.fragments.cases.viewmodel.CaseViewModel
import com.easipos.helpdesk.fragments.cases.viewmodel.CaseViewModelFactory
import com.easipos.helpdesk.fragments.dialog_fragment.RateCaseDialogFragment
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.models.CustomerOutlet
import com.easipos.helpdesk.models.Selectable
import com.easipos.helpdesk.tools.EqualSpacingItemDecoration
import com.easipos.helpdesk.util.showBottomSheet
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder
import io.github.anderscheow.library.constant.EventBusType
import io.github.anderscheow.library.kotlinExt.*
import io.github.anderscheow.library.viewModel.observeItems
import io.github.anderscheow.library.viewModel.observeNetworkState
import io.github.anderscheow.library.viewModel.observeTotalItems
import kotlinx.android.synthetic.main.fragment_case.*
import kotlinx.android.synthetic.main.nav_view_filter.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.kodein.di.generic.instance
import java.util.*

class CaseFragment : CustomLifecycleFragment<CaseViewModel>(), CaseView,
    CaseAdapter.OnGestureDetectedListener, SelectionAdapter.OnGestureDetectedListener {

    companion object {
        fun newInstance(): CaseFragment {
            return CaseFragment()
        }
    }

    //region Variables
    private val navigation by instance<CaseNavigation>()

    private val presenter by lazy { CasePresenter(requireActivity().application) }
    private val factory by lazy { CaseViewModelFactory(requireActivity().application) }

    private var selectedDateFrom: Calendar? = null
    private var selectedDateTo: Calendar? = null
    private var selectedStatusList = arrayListOf<CaseStatus>()
    private var selectedOutletList = arrayListOf<CustomerOutlet>()

    private var selectionDialog: BottomSheetDialog? = null

    private val itemDecoration by lazy { EqualSpacingItemDecoration(resources.getDimensionPixelSize(R.dimen.sixteen_dp), EqualSpacingItemDecoration.VERTICAL) }
    //endregion

    //region Lifecycle
//    override fun onResume() {
//        super.onResume()
//        getCases()
//    }
    //endregion

    //region CustomLifecycleFragment Abstract Methods
    override fun getResLayout(): Int = R.layout.fragment_case

    override fun getEventBusType(): EventBusType? = EventBusType.ON_ATTACH

    override fun setupViewModel(): CaseViewModel {
        return ViewModelProvider(this, factory)
            .get(CaseViewModel::class.java)
    }

    override fun setupViewModelObserver(lifecycleOwner: LifecycleOwner) {
    }

    override fun init() {
        super.init()
        presenter.onAttachView(this)

        setupViews()
        setupListeners()

        getCases()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        withContext { context ->
            CommonAlertDialog(context, message, action).show()
        }
    }

    override fun showCustomerOutletSelections(customerOutlets: List<CustomerOutlet>) {
        val selectableList = customerOutlets.map { Selectable(it as Any) }
        selectedOutletList.forEach { selectedOutlet ->
            val selectable = selectableList.find {
                (it.data as CustomerOutlet).outletCode == selectedOutlet.outletCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_outlet), selectableList)
    }
    //endregion

    //region Interface methods
    override fun onSelectItem(item: Case) {
        withActivity { activity ->
            navigation.navigateToViewCase(activity, item.caseId)
        }
    }

    override fun onRateCase(item: Case) {
        withActivityAs<MainActivity> {
            RateCaseDialogFragment.show(it, item)
        }
    }

    override fun onSelectItem(position: Int, data: Any) {
    }

    override fun onReturnItems(items: List<Any>) {
        if (items.isNotEmpty()) {
            when (items[0]) {
                is CaseStatus -> {
                    selectedStatusList.clear()
                    items.forEach { selectedStatusList.add(it as CaseStatus) }

                    text_view_status_filter.text = selectedStatusList.joinToString("\n") { it.value }

                    image_view_close_status.visible()
                }

                is CustomerOutlet -> {
                    selectedOutletList.clear()
                    items.forEach { selectedOutletList.add(it as CustomerOutlet) }

                    text_view_outlet_filter.text = selectedOutletList.joinToString("\n\n") { it.toString() }

                    image_view_close_outlet.visible()
                }
            }
        }
    }
    //endregion

    //region Action methods
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: Any) {
        if (event is RefreshCaseListing) {
            viewModel.onRefresh()
        }
    }

    fun getCases() {
        isAdded {
            if (selectedDateFrom != null && selectedDateTo == null) {
                showErrorAlertDialog(getString(R.string.error_date_to_required))
                return@isAdded
            }

            if (selectedDateTo != null && selectedDateFrom == null) {
                showErrorAlertDialog(getString(R.string.error_date_from_required))
                return@isAdded
            }

            val model = GetCasesRequestModel(
                dateFrom = if (selectedDateFrom == null) null else selectedDateFrom?.timeInMillis?.formatDate(
                    "dd/MM/yyyy"
                ),
                dateTo = if (selectedDateTo == null) null else selectedDateTo?.timeInMillis?.formatDate(
                    "dd/MM/yyyy"
                ),
                status = selectedStatusList.map { it.name },
                outletCode = selectedOutletList.map { it.outletCode }
            )

            viewModel.start(model)
            setupAdapter()

            closeDrawer()

        }
    }

    private fun setupViews() {
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    private fun setupListeners() {
        text_view_filter.click {
            drawer_layout.openDrawer(GravityCompat.END)
        }

        card_view_date_from.click {
            showDateSelector(0)
        }

        card_view_date_to.click {
            showDateSelector(1)
        }

        card_view_status.click {
            showStatusSelections()
        }

        card_view_outlet.click {
            presenter.doGetCustomerOutlets(GetCustomerOutletsRequestModel())
        }

        image_view_close_date_range.click {
            resetDateRangeFilter()
        }

        image_view_close_status.click {
            resetStatusFilter()
        }

        image_view_close_outlet.click {
            resetOutletFilter()
        }

        button_reset.click {
            resetAllFilters()
        }

        button_apply.click {
            getCases()
        }
    }

    private fun setupAdapter() {
        withContext { context ->
            val adapter = CaseAdapter(this) { viewModel.retry() }

            setupRecyclerView(context, adapter)
            setupViewModel(adapter)
        }
    }

    private fun setupRecyclerView(context: Context, adapter: CaseAdapter) {
        recycler_view_case.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
            this.removeItemDecoration(itemDecoration)
            this.addItemDecoration(itemDecoration)
        }
    }

    private fun setupViewModel(adapter: CaseAdapter) {
        with(viewModel) {
            this.observeItems(this@CaseFragment, adapter)
            this.observeNetworkState(this@CaseFragment, adapter, true)
            this.observeTotalItems(this@CaseFragment)
        }
    }

    private fun closeDrawer() {
        if (drawer_layout.isDrawerOpen(GravityCompat.END)) {
            drawer_layout.closeDrawer(GravityCompat.END)
        }
    }

    private fun resetAllFilters() {
        selectedDateFrom = null
        selectedDateTo = null
        selectedStatusList.clear()
        selectedOutletList.clear()

        text_view_status_filter.text = null
        text_view_date_from_filter.setText(R.string.label_from)
        text_view_date_to_filter.setText(R.string.label_to)
        text_view_outlet_filter.text = null

        image_view_close_date_range.invisible()
        image_view_close_status.invisible()
        image_view_close_outlet.invisible()

        getCases()
    }

    private fun resetDateRangeFilter() {
        selectedDateFrom = null
        selectedDateTo = null

        text_view_date_from_filter.setText(R.string.label_from)
        text_view_date_to_filter.setText(R.string.label_to)

        image_view_close_date_range.invisible()
    }

    private fun resetStatusFilter() {
        selectedStatusList.clear()

        text_view_status_filter.text = null

        image_view_close_status.invisible()
    }

    private fun resetOutletFilter() {
        selectedOutletList.clear()

        text_view_outlet_filter.text = null

        image_view_close_outlet.invisible()
    }

    private fun showDateSelector(dateType: Int) {
        withContext { context ->
            SpinnerDatePickerDialogBuilder()
                .context(context)
                .spinnerTheme(R.style.NumberPickerStyle)
                .callback { _, year, monthOfYear, dayOfMonth ->
                    val calendar = Calendar.getInstance().apply {
                        this.set(year, monthOfYear, dayOfMonth)
                    }

                    if (dateType == 0) {
                        selectedDateFrom = calendar

                        text_view_date_from_filter.text = calendar.timeInMillis.formatDate("dd/MM/yyyy")
                    } else {
                        selectedDateTo = calendar

                        text_view_date_to_filter.text = calendar.timeInMillis.formatDate("dd/MM/yyyy")
                    }
                }
                .showTitle(true)
                .showDaySpinner(true)
                .apply {
                    if (dateType == 0) {
                        // From Date
                        val from = selectedDateFrom ?: Calendar.getInstance()
                        val to = selectedDateTo ?: Calendar.getInstance()

                        this.defaultDate(from.get(Calendar.YEAR), from.get(Calendar.MONTH), from.get(Calendar.DAY_OF_MONTH))
                        this.maxDate(to.get(Calendar.YEAR), to.get(Calendar.MONTH), to.get(Calendar.DAY_OF_MONTH))
                    } else {
                        // To Date
                        val from = selectedDateFrom ?: Calendar.getInstance()
                        val to = selectedDateTo ?: Calendar.getInstance()
                        val max = Calendar.getInstance().apply {
                            this.add(Calendar.DAY_OF_MONTH, 1)
                        }

                        this.defaultDate(to.get(Calendar.YEAR), to.get(Calendar.MONTH), to.get(Calendar.DAY_OF_MONTH))
                        this.minDate(from.get(Calendar.YEAR), from.get(Calendar.MONTH), from.get(Calendar.DAY_OF_MONTH))
                        this.maxDate(max.get(Calendar.YEAR), max.get(Calendar.MONTH), max.get(Calendar.DAY_OF_MONTH))
                    }
                }
                .build()
                .show()
        }
    }

    private fun showStatusSelections() {
        val selectableList = CaseStatus.toList(true).map { Selectable(it as Any) }
        selectedStatusList.forEach { selectedStatus ->
            val selectable = selectableList.find {
                (it.data as CaseStatus).name == selectedStatus.name
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_case_status), selectableList)
    }

    private fun showBottomSheetSelection(title: String,
                                         data: List<Selectable<Any>>,
                                         selectionType: SelectionAdapter.SelectionType = SelectionAdapter.SelectionType.MULTIPLE) {
        withActivity { activity ->
            selectionDialog?.dismiss()
            selectionDialog = showBottomSheet(activity, this, title, data, selectionType)
            selectionDialog?.show()
        }
    }
    //endregion
}