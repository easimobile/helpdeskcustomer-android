package com.easipos.helpdesk.fragments.account.navigation

import android.app.Activity

interface AccountNavigation {

    fun navigateToChangePassword(activity: Activity)
}