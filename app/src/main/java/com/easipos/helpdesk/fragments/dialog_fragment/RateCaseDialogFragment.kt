package com.easipos.helpdesk.fragments.dialog_fragment

import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.main.MainActivity
import com.easipos.helpdesk.api.requests.cases.SubmitRatingRequestModel
import com.easipos.helpdesk.base.CustomBaseDialogFragment
import com.easipos.helpdesk.bundle.ParcelData
import com.easipos.helpdesk.models.Case
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.dialog_fragment_rate_case.*
import org.jetbrains.anko.displayMetrics

class RateCaseDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun show(foundationAppCompatActivity: FoundationAppCompatActivity, case: Case) {
            foundationAppCompatActivity.removeDialogFragmentThen(RateCaseDialogFragment.TAG) {
                RateCaseDialogFragment().apply {
                    this.arguments = Bundle().apply {
                        this.putParcelable(ParcelData.CASE, case)
                    }
                }
            }
        }
    }

    private val case by argument<Case>(ParcelData.CASE)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        withActivity { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.9).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_rate_case

    override fun init() {
        super.init()

        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        text_view_maybe_next_time.paintFlags = text_view_maybe_next_time.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        text_view_case_id.text = case?.formatCaseId()
        text_view_reported_date.text = case?.formatCreatedDate()
        text_view_closed_date.text = case?.formatClosedDate()
    }

    private fun setupListeners() {
        button_submit.click {
            attemptSubmitRating()
        }

        text_view_maybe_next_time.click {
            dismissAllowingStateLoss()
        }
    }

    private fun attemptSubmitRating() {
        case?.let { nonRatedCase ->
            val rating = rating_bar.rating.toInt()
            val remark = text_input_edit_text_remark.text?.toString() ?: ""

            val model = SubmitRatingRequestModel(
                caseId = nonRatedCase.caseId,
                rate = rating,
                remark = remark
            )

            withActivityAs<MainActivity>()?.submitRating(this, model)
        }
    }
}
