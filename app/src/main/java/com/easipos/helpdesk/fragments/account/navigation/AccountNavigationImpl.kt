package com.easipos.helpdesk.fragments.account.navigation

import android.app.Activity
import com.easipos.helpdesk.activities.main.MainActivity

class AccountNavigationImpl : AccountNavigation {

    override fun navigateToChangePassword(activity: Activity) {
        (activity as? MainActivity)?.navigateToChangePassword()
    }
}