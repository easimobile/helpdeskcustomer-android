package com.easipos.helpdesk.fragments.cases.navigation

import android.app.Activity
import com.easipos.helpdesk.activities.main.MainActivity

class CaseNavigationImpl : CaseNavigation {

    override fun navigateToViewCase(activity: Activity, caseId: String) {
        (activity as? MainActivity)?.navigateToViewCase(caseId)
    }
}