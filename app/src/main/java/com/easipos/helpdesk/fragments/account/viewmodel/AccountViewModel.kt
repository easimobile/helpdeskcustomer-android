package com.easipos.helpdesk.fragments.account.viewmodel

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import com.easipos.helpdesk.base.CustomBaseAndroidViewModel
import com.easipos.helpdesk.models.CustomerInfo
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.user.UserRepository
import com.easipos.helpdesk.tools.Preference
import io.github.anderscheow.library.kotlinExt.delay
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance

class AccountViewModel(application: Application) : CustomBaseAndroidViewModel<Void>(application) {

    //region Observable Field
    val customerInfo = ObservableField<CustomerInfo>()
    //endregion

    //region LiveData
    //endregion

    private val userRepository by instance<UserRepository>()

    override fun start(args: Void?) {
        getUserInfo()
    }

    fun getUserInfo() {
        setIsLoading(true)
        viewModelScope.launch {
            when (val result = userRepository.getCustomerInfo()) {
                is Result.Success<CustomerInfo> -> {
                    setIsLoading(false)
                    Preference.prefCustomerInfo = result.data
                    setCustomerInfo(result.data)
                }

                is Result.Error -> {
                    delay(2000) {
                        getUserInfo()
                    }
                }

                else -> {
                }
            }
        }
    }

    private fun setCustomerInfo(customerInfo: CustomerInfo) {
        this.customerInfo.set(customerInfo)
        this.customerInfo.notifyChange()
    }
}