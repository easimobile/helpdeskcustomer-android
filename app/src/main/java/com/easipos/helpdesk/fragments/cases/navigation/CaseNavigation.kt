package com.easipos.helpdesk.fragments.cases.navigation

import android.app.Activity

interface CaseNavigation {

    fun navigateToViewCase(activity: Activity, caseId: String)
}