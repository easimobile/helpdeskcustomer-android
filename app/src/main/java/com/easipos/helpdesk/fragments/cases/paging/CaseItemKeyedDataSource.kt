package com.easipos.helpdesk.fragments.cases.paging

import com.easipos.helpdesk.api.requests.cases.GetCasesRequestModel
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.models.DataSourceListModel
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import io.github.anderscheow.library.paging.remote.BaseItemKeyedDataSource
import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.Executor
import kotlin.coroutines.CoroutineContext

class CaseItemKeyedDataSource internal constructor(retryExecutor: Executor,
                                                   private val model: GetCasesRequestModel,
                                                   private val caseRepository: CaseRepository)
    : BaseItemKeyedDataSource<String, Case>(retryExecutor), CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    override fun loadInitial(success: (List<Case>, Long) -> Unit, failed: (String) -> Unit) {
        val group = ArrayList<Case>()

        model.page = pageNumber

        launch {
            val result = caseRepository.getCases(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<DataSourceListModel<Case>> -> {
                        hasNext = result.data.hasNext
                        group.addAll(result.data.itemList)
                        success.invoke(group, result.data.totalOfElements)
                    }

                    is Result.Error -> {
                        failed.invoke(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    override fun loadAfter(success: (List<Case>) -> Unit, failed: (String) -> Unit) {
        val group = ArrayList<Case>()

        model.page = pageNumber

        launch {
            val result = caseRepository.getCases(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<DataSourceListModel<Case>> -> {
                        hasNext = result.data.hasNext
                        group.addAll(result.data.itemList)
                        success.invoke(group)
                    }

                    is Result.Error -> {
                        failed.invoke(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    override fun getKey(item: Case): String {
        return item.caseId
    }
    
    fun onCleared() {
        job.cancel()
    }
}
