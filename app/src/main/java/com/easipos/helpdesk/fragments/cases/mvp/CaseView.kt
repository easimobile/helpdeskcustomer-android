package com.easipos.helpdesk.fragments.cases.mvp

import com.easipos.helpdesk.base.View
import com.easipos.helpdesk.models.CustomerOutlet

interface CaseView : View {

    fun showCustomerOutletSelections(customerOutlets: List<CustomerOutlet>)
}
