package com.easipos.helpdesk.fragments.cases.mvp

import android.app.Application
import com.easipos.helpdesk.api.requests.cases.GetCustomerOutletsRequestModel
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.models.CustomerOutlet
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class CasePresenter(application: Application)
    : Presenter<CaseView>(application) {

    private val caseRepository by instance<CaseRepository>()

    fun doGetCustomerOutlets(model: GetCustomerOutletsRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCustomerOutlets(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<CustomerOutlet>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showCustomerOutletSelections(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }
}
