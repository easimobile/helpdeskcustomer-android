package com.easipos.helpdesk.di.modules

import com.easipos.helpdesk.activities.change_password.navigation.ChangePasswordNavigation
import com.easipos.helpdesk.activities.change_password.navigation.ChangePasswordNavigationImpl
import com.easipos.helpdesk.activities.login.navigation.LoginNavigation
import com.easipos.helpdesk.activities.login.navigation.LoginNavigationImpl
import com.easipos.helpdesk.activities.main.navigation.MainNavigation
import com.easipos.helpdesk.activities.main.navigation.MainNavigationImpl
import com.easipos.helpdesk.activities.reset_password.navigation.ResetPasswordNavigation
import com.easipos.helpdesk.activities.reset_password.navigation.ResetPasswordNavigationImpl
import com.easipos.helpdesk.activities.splash.navigation.SplashNavigation
import com.easipos.helpdesk.activities.splash.navigation.SplashNavigationImpl
import io.github.anderscheow.library.di.modules.ActivityBaseModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

class ActivityModule : ActivityBaseModule() {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<SplashNavigation>() with provider { SplashNavigationImpl() }
            bind<LoginNavigation>() with provider { LoginNavigationImpl() }
            bind<ResetPasswordNavigation>() with provider { ResetPasswordNavigationImpl() }
            bind<MainNavigation>() with provider { MainNavigationImpl() }
            bind<ChangePasswordNavigation>() with provider { ChangePasswordNavigationImpl() }
        }
    }
}
