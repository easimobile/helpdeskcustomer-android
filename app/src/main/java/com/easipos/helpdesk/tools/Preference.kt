package com.easipos.helpdesk.tools

import com.easipos.helpdesk.BuildConfig
import com.easipos.helpdesk.models.CustomerInfo
import com.google.gson.Gson
import com.pixplicity.easyprefs.library.Prefs

object Preference {

    private const val PREF_LANGUAGE_CODE = "PREF_LANGUAGE_CODE"
    private const val PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN"
    private const val PREF_IS_LOGGED_IN = "PREF_IS_LOGGED_IN"
    private const val PREF_IS_FCM_REGISTERED = "PREF_IS_FCM_REGISTERED"
    private const val PREF_FCM_TOKEN = "PREF_FCM_TOKEN"
    private const val PREF_NOTIFICATION_COUNT = "PREF_NOTIFICATION_COUNT"
    private const val PREF_COMPANY_CODE = "PREF_COMPANY_CODE"
    private const val PREF_USER_CODE = "PREF_USER_CODE"
    private const val PREF_PROJECT_CODE = "PREF_PROJECT_CODE"
    private const val PREF_CLIENT_NAME = "PREF_CLIENT_NAME"
    private const val PREF_CLIENT_URL = "PREF_CLIENT_URL"
    private const val PREF_CUSTOMER_INFO = "PREF_CUSTOMER_INFO"

    private val gson = Gson()

    var prefLanguageCode: String
        get() = Prefs.getString(PREF_LANGUAGE_CODE, "en")
        set(languageCode) = Prefs.putString(PREF_LANGUAGE_CODE, languageCode)

    var prefAccessToken: String
        get() = Prefs.getString(PREF_ACCESS_TOKEN, "")
        set(accessToken) = Prefs.putString(PREF_ACCESS_TOKEN, accessToken)

    var prefIsLoggedIn: Boolean
        get() = Prefs.getBoolean(PREF_IS_LOGGED_IN, false)
        set(isLoggedIn) = Prefs.putBoolean(PREF_IS_LOGGED_IN, isLoggedIn)

    var prefIsFcmTokenRegistered: Boolean
        get() = Prefs.getBoolean(PREF_IS_FCM_REGISTERED, false)
        set(isRegistered) = Prefs.putBoolean(PREF_IS_FCM_REGISTERED, isRegistered)

    var prefFcmToken: String
        get() = Prefs.getString(PREF_FCM_TOKEN, "")
        set(token) = Prefs.putString(PREF_FCM_TOKEN, token)

    var prefNotificationCount: Int
        get() = Prefs.getInt(PREF_NOTIFICATION_COUNT, 0)
        set(count) = Prefs.putInt(PREF_NOTIFICATION_COUNT, count)

    var prefCompanyCode: String
        get() = Prefs.getString(PREF_COMPANY_CODE, "")
        set(companyCode) = Prefs.putString(PREF_COMPANY_CODE, companyCode)

    var prefUserCode: String
        get() = Prefs.getString(PREF_USER_CODE, "")
        set(userCode) = Prefs.putString(PREF_USER_CODE, userCode)

    var prefProjectCode: String
        get() = Prefs.getString(PREF_PROJECT_CODE, "")
        set(projectCode) = Prefs.putString(PREF_PROJECT_CODE, projectCode)

    var prefClientName: String
        get() = Prefs.getString(PREF_CLIENT_NAME, "")
        set(clientName) = Prefs.putString(PREF_CLIENT_NAME, clientName)

    var prefClientUrl: String
        get() = Prefs.getString(PREF_CLIENT_URL, BuildConfig.API_DOMAIN)
        set(clientUrl) = Prefs.putString(PREF_CLIENT_URL, clientUrl)

    var prefCustomerInfo: CustomerInfo?
        get() {
            val json = Prefs.getString(PREF_CUSTOMER_INFO, "{}")
            return if (json != null) {
                gson.fromJson(json, CustomerInfo::class.java)
            } else {
                null
            }
        }
        set(customerInfo) = Prefs.putString(PREF_CUSTOMER_INFO, gson.toJson(customerInfo))

    fun logout() {
        prefIsLoggedIn = false
        prefIsFcmTokenRegistered = false
        prefNotificationCount = 0
        prefUserCode = ""
        prefProjectCode = ""

        Prefs.remove(PREF_ACCESS_TOKEN)
    }
}