package com.easipos.helpdesk.tools

import android.graphics.drawable.Drawable
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import com.easipos.helpdesk.R
import com.easipos.helpdesk.constant.CasePriority
import com.easipos.helpdesk.constant.CaseStatus
import com.google.android.material.card.MaterialCardView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textview.MaterialTextView
import com.willy.ratingbar.ScaleRatingBar
import io.github.anderscheow.library.kotlinExt.findColor
import org.jetbrains.anko.textColor

object BindingAdapterUtil {

    @BindingAdapter("android:layout_marginStart")
    @JvmStatic
    fun layoutMarginStart(cardView: MaterialCardView, dimen: Float) {
        val layoutParams = cardView.layoutParams as ViewGroup.MarginLayoutParams
        layoutParams.marginStart = dimen.toInt()
        cardView.layoutParams = layoutParams
    }

    @BindingAdapter("android:layout_marginTop")
    @JvmStatic
    fun layoutMarginTop(cardView: MaterialCardView, dimen: Float) {
        val layoutParams = cardView.layoutParams as ViewGroup.MarginLayoutParams
        layoutParams.topMargin = dimen.toInt()
        cardView.layoutParams = layoutParams
    }

    @BindingAdapter("android:drawableEnd")
    @JvmStatic
    fun drawableEnd(textView: MaterialTextView, drawable: Drawable?) {
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, drawable, null)
    }

    @BindingAdapter("srcCompat")
    @JvmStatic
    fun srcCompat(fab: FloatingActionButton, drawable: Drawable) {
        fab.setImageDrawable(drawable)
    }

    @BindingAdapter("setStatusBackground")
    @JvmStatic
    fun setStatusBackground(textView: MaterialTextView, status: String?) {
        CaseStatus.parse(status ?: "")?.let {
            when (it) {
                CaseStatus.OPEN -> {
                    textView.setBackgroundResource(R.drawable.bg_status_open)
                }

                CaseStatus.IN_PROGRESS -> {
                    textView.setBackgroundResource(R.drawable.bg_status_in_progress)
                }

                CaseStatus.PENDING -> {
                    textView.setBackgroundResource(R.drawable.bg_status_pending)
                }

                CaseStatus.RESOLVED -> {
                    textView.setBackgroundResource(R.drawable.bg_status_resolved)
                }

                CaseStatus.CLOSED -> {
                    textView.setBackgroundResource(R.drawable.bg_status_closed)
                }

                CaseStatus.CLOSED_WITH_EXCEPTION -> {
                    textView.setBackgroundResource(R.drawable.bg_status_closed_exception)
                }

                CaseStatus.DUPLICATE -> {
                    textView.setBackgroundResource(R.drawable.bg_status_duplicate)
                }
            }
        }
    }

    @BindingAdapter("setStatusBackground2")
    @JvmStatic
    fun setStatusBackground2(textView: MaterialTextView, status: String?) {
        CaseStatus.parse(status ?: "")?.let {
            when (it) {
                CaseStatus.OPEN -> {
                    textView.setBackgroundResource(R.drawable.bg_status_open_2)
                }

                CaseStatus.IN_PROGRESS -> {
                    textView.setBackgroundResource(R.drawable.bg_status_in_progress_2)
                }

                CaseStatus.PENDING -> {
                    textView.setBackgroundResource(R.drawable.bg_status_pending_2)
                }

                CaseStatus.RESOLVED -> {
                    textView.setBackgroundResource(R.drawable.bg_status_resolved_2)
                }

                CaseStatus.CLOSED -> {
                    textView.setBackgroundResource(R.drawable.bg_status_closed_2)
                }

                CaseStatus.CLOSED_WITH_EXCEPTION -> {
                    textView.setBackgroundResource(R.drawable.bg_status_closed_exception_2)
                }

                CaseStatus.DUPLICATE -> {
                    textView.setBackgroundResource(R.drawable.bg_status_duplicate_2)
                }
            }
        }
    }

    @BindingAdapter("setPriorityIconAndTextColor")
    @JvmStatic
    fun setPriorityIconAndTextColor(textView: MaterialTextView, priority: Int?) {
        CasePriority.parse(priority ?: -1)?.let {
            when (it) {
                CasePriority.LOW -> {
                    textView.textColor = textView.context.findColor(R.color.colorPriorityLow)
                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_priority_low, 0)
                }

                CasePriority.MEDIUM -> {
                    textView.textColor = textView.context.findColor(R.color.colorPriorityMedium)
                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_priority_medium, 0)
                }

                CasePriority.HIGH -> {
                    textView.textColor = textView.context.findColor(R.color.colorPriorityHigh)
                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_priority_high, 0)
                }

                CasePriority.CRITICAL -> {
                    textView.textColor = textView.context.findColor(R.color.colorPriorityCritical)
                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_priority_critical, 0)
                }
            }
        }
    }

    @BindingAdapter("srb_rating")
    @JvmStatic
    fun srb_rating(ratingBar: ScaleRatingBar, rating: Int) {
        ratingBar.rating = rating.toFloat()
    }
}