package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager
import com.easipos.helpdesk.tools.Preference
import com.google.gson.annotations.SerializedName

data class GetCasesRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val mobile: Int = 1,
    var page: Int = 1,

    @SerializedName("Project")
    val projectCode: String = Preference.prefProjectCode,

    @SerializedName("DateFrom")
    val dateFrom: String? = null,

    @SerializedName("DateTo")
    val dateTo: String? = null,

    @SerializedName("Status")
    val status: List<String>,

    @SerializedName("OutletCode")
    val outletCode: List<String>
)