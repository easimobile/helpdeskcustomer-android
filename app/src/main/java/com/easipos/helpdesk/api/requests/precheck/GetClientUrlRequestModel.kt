package com.easipos.helpdesk.api.requests.precheck

import com.easipos.helpdesk.tools.Preference

data class GetClientUrlRequestModel(
    val clientName: String = Preference.prefClientName
)