package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager
import com.easipos.helpdesk.tools.Preference

data class GetCustomerOutletsRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val projectCode: String = Preference.prefProjectCode
)