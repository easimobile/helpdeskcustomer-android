package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class CaseSubTypeResponseModel(
    @SerializedName("CaseSubTypeCode")
    val caseSubTypeCode: String?,

    @SerializedName("CaseSubTypeDesc")
    val caseSubTypeDesc: String?,

    @SerializedName("hasCaseSubType")
    val hasCaseSubType: String?
)