package com.easipos.helpdesk.api.services

import com.easipos.helpdesk.api.ApiEndpoint
import com.easipos.helpdesk.api.misc.EmptyResponseModel
import com.easipos.helpdesk.api.misc.ResponseModel
import com.easipos.helpdesk.api.requests.auth.LoginRequestModel
import com.easipos.helpdesk.api.requests.cases.CreateCaseRequestModel
import com.easipos.helpdesk.api.requests.cases.GetCasesRequestModel
import com.easipos.helpdesk.api.requests.cases.SubmitRatingRequestModel
import com.easipos.helpdesk.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.helpdesk.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.helpdesk.api.requests.user.ChangePasswordRequestModel
import com.easipos.helpdesk.api.requests.user.ForgotPasswordRequestModel
import com.easipos.helpdesk.api.responses.auth.LoginInfoResponseModel
import com.easipos.helpdesk.api.responses.cases.*
import com.easipos.helpdesk.api.responses.user.CustomerInfoResponseModel
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {

    @POST(ApiEndpoint.CHECK_VERSION)
    suspend fun checkVersion(@Body body: RequestBody): ResponseModel<Boolean>

    @POST(ApiEndpoint.LOGIN)
    suspend fun login(@Body body: LoginRequestModel): ResponseModel<LoginInfoResponseModel>

    @POST(ApiEndpoint.FORGOT_PASSWORD)
    suspend fun forgotPassword(@Body body: ForgotPasswordRequestModel): EmptyResponseModel

    @POST(ApiEndpoint.CHANGE_PASSWORD)
    suspend fun changePassword(@Body body: ChangePasswordRequestModel): EmptyResponseModel

    @GET(ApiEndpoint.GET_CUSTOMER_INFO)
    suspend fun getUserInfo(@Query("apiKey") apiKey: String): ResponseModel<CustomerInfoResponseModel>

    @POST(ApiEndpoint.GET_CASES)
    suspend fun getCases(@Body body: GetCasesRequestModel): ResponseModel<CasePagingResponseModel>

    @GET(ApiEndpoint.GET_CASE_INFO)
    suspend fun getCaseInfo(@Query("apiKey") apiKey: String,
                            @Query("CaseID") caseId: String,
                            @Query("isCustomer") isCustomer: Boolean): ResponseModel<CaseInfoResponseModel>

    @GET(ApiEndpoint.GET_CUSTOMER_OUTLETS)
    suspend fun getCustomerOutlets(@Query("apiKey") apiKey: String,
                                   @Query("projectCode") projectCode: String): ResponseModel<List<CustomerOutletResponseModel>>

    @GET(ApiEndpoint.GET_OUTLET_POS_LIST)
    suspend fun getOutletPosList(@Query("apiKey") apiKey: String,
                                 @Query("outletCode") outletCode: String): ResponseModel<List<OutletPosResponseModel>>

    @GET(ApiEndpoint.GET_CASE_SERVICE_TYPES)
    suspend fun getServiceTypes(@Query("apiKey") apiKey: String): ResponseModel<List<ServiceTypeResponseModel>>

    @GET(ApiEndpoint.GET_CASE_TYPES)
    suspend fun getCaseTypes(@Query("apiKey") apiKey: String): ResponseModel<List<CaseTypeResponseModel>>

    @GET(ApiEndpoint.GET_CASE_SUB_TYPES_1)
    suspend fun getCaseSubTypes1(@Query("apiKey") apiKey: String,
                                 @Query("CaseTypeCode") code: String): ResponseModel<List<CaseSubTypeResponseModel>>

    @POST(ApiEndpoint.CREATE_CASE)
    suspend fun createCase(@Body body: CreateCaseRequestModel): ResponseModel<String>

    @POST(ApiEndpoint.SUBMIT_RATING)
    suspend fun submitRating(@Body body: SubmitRatingRequestModel): EmptyResponseModel

    @POST(ApiEndpoint.REGISTER_PUSH_TOKEN)
    suspend fun registerFcmToken(@Body body: RegisterFcmTokenRequestModel): EmptyResponseModel

    @POST(ApiEndpoint.REMOVE_PUSH_TOKEN)
    suspend fun removeFcmToken(@Body body: RemoveFcmTokenRequestModel): EmptyResponseModel
}
