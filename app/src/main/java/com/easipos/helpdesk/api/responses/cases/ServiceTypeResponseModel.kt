package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class ServiceTypeResponseModel(
    @SerializedName("ServiceTypeCode")
    val serviceTypeCode: String?,

    @SerializedName("ServiceTypeDesc")
    val serviceTypeDesc: String?
)