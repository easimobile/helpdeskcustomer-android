package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class OutletPosResponseModel(
    @SerializedName("POS_NO")
    val posNo: String?,

    @SerializedName("POS_DESC")
    val posDesc: String?,

    @SerializedName("IP_ADDRESS")
    val ipAddress: String?
)