package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class CustomerOutletResponseModel(
    @SerializedName("outletCode")
    val outletCode: String?,

    @SerializedName("outletName")
    val outletName: String?,

    @SerializedName("Addr1")
    val address1: String?,

    @SerializedName("Addr2")
    val address2: String?,

    @SerializedName("Addr3")
    val address3: String?,

    @SerializedName("Addr4")
    val address4: String?,

    @SerializedName("Addr5")
    val address5: String?,

    @SerializedName("postalCode")
    val postalCode: String?,

    @SerializedName("posCount")
    val posCount: Int?
)