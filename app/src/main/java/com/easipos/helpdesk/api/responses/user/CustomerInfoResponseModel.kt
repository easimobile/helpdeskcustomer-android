package com.easipos.helpdesk.api.responses.user

import com.google.gson.annotations.SerializedName

data class CustomerInfoResponseModel(
    @SerializedName("CustomerName")
    val customerName: String?,

    @SerializedName("ProjectName")
    val projectName: String?,

    @SerializedName("ContactPerson")
    val contactPerson: String?,

    @SerializedName("ContactNo")
    val contactNo: String?
)