package com.easipos.helpdesk.api

object ApiEndpoint {

    const val GET_CLIENT_URL = "getClientUrl"

    const val CHECK_VERSION = "ddcard/version/check"

    const val LOGIN = "login_customer"

    const val FORGOT_PASSWORD = "forgotPassword_customer"
    const val CHANGE_PASSWORD = "changePassword_customer"
    const val GET_CUSTOMER_INFO = "api/v1/getCustomerInfo"

    const val GET_CASES = "api/v1/Case/getAllCases_customer"
    const val GET_CASE_INFO = "api/v1/Case/getCaseInfo"
    const val CREATE_CASE = "api/v1/Case/addCase_customer"
    const val GET_CUSTOMER_OUTLETS = "api/v1/Case/getCustomerOutlets"
    const val GET_OUTLET_POS_LIST = "api/v1/Case/getOutletPos"
    const val GET_CASE_SERVICE_TYPES = "api/v1/Case/getCaseServiceType"
    const val GET_CASE_TYPES = "api/v1/Case/getCaseType_customer"
    const val GET_CASE_SUB_TYPES_1 = "api/v1/Case/getCaseSubType1"
    const val SUBMIT_RATING = "api/v1/Case/submitRating"

    const val REGISTER_PUSH_TOKEN = "api/v1/Notification/registerPushToken"
    const val REMOVE_PUSH_TOKEN = "api/v1/Notification/removePushToken"
}
