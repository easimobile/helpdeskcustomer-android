package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager

data class GetCaseInfoRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val caseId: String,
    val isCustomer: Boolean = true
)