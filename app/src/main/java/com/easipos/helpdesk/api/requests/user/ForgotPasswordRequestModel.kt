package com.easipos.helpdesk.api.requests.user

data class ForgotPasswordRequestModel(
    val mobile: Int = 1,
    val id: String
)