package com.easipos.helpdesk.api.requests.auth

data class LoginRequestModel(
    val username: String,
    val password: String,
    val companyCode: String,
    val mobile: Int = 1
)