package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class CaseTypeResponseModel(
    @SerializedName("CaseTypeCode")
    val caseTypeCode: String?,

    @SerializedName("CaseTypeDesc")
    val caseTypeDesc: String?,

    @SerializedName("hasCaseSubType")
    val hasCaseSubType: String?
)