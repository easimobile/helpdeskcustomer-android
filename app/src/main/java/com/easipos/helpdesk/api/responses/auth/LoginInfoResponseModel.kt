package com.easipos.helpdesk.api.responses.auth

import com.google.gson.annotations.SerializedName

data class LoginInfoResponseModel(
    @SerializedName("apiKey")
    val apiKey: String?,

    @SerializedName("projectCode")
    val projectCode: String?
)