package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager

data class GetCaseSubTypesRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val code: String
)