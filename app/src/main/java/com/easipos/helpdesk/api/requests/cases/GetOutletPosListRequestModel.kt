package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager

data class GetOutletPosListRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val outletCode: String
)