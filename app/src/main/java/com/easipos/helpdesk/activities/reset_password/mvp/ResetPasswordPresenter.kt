package com.easipos.helpdesk.activities.reset_password.mvp

import android.app.Application
import com.easipos.helpdesk.R
import com.easipos.helpdesk.api.requests.user.ForgotPasswordRequestModel
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.user.UserRepository
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class ResetPasswordPresenter(application: Application)
    : Presenter<ResetPasswordView>(application) {

    private val userRepository by instance<UserRepository>()

    fun doForgotPassword(model: ForgotPasswordRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = userRepository.forgotPassword(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.EmptySuccess -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(application.getString(R.string.prompt_reset_password_email_sent)) {
                            view?.finishScreen()
                        }
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }
}
