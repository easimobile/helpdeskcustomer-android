package com.easipos.helpdesk.activities.login.navigation

import android.app.Activity
import com.easipos.helpdesk.activities.change_password.ChangePasswordActivity
import com.easipos.helpdesk.activities.main.MainActivity
import com.easipos.helpdesk.activities.reset_password.ResetPasswordActivity

class LoginNavigationImpl : LoginNavigation {

    override fun navigateToResetPassword(activity: Activity) {
        activity.startActivity(ResetPasswordActivity.newIntent(activity))
    }

    override fun navigateToChangePassword(activity: Activity) {
        activity.startActivity(ChangePasswordActivity.newIntent(activity))
    }

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}