package com.easipos.helpdesk.activities.main.mvp

import android.app.Application
import androidx.fragment.app.DialogFragment
import com.easipos.helpdesk.Easi
import com.easipos.helpdesk.api.requests.cases.SubmitRatingRequestModel
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.managers.FcmManager
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import io.github.anderscheow.library.kotlinExt.delay
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class MainPresenter(application: Application)
    : Presenter<MainView>(application) {

    private val fcmManager by instance<FcmManager>()

    private val caseRepository by instance<CaseRepository>()

    fun doLogout() {
        fcmManager.service.removeFcmToken()

        delay(1000) {
            (application as? Easi)?.logout()
            view?.navigateToSplash()
        }
    }

    fun doSubmitRating(dialogFragment: DialogFragment, model: SubmitRatingRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.submitRating(model)
            withContext(Dispatchers.Main) {
                view?.setLoadingIndicator(false)
                when (result) {
                    is Result.EmptySuccess -> {
                        view?.markCaseAsRated(dialogFragment, model.caseId)
                    }

                    is Result.Error -> {
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {}
                }
            }
        }
    }
}
