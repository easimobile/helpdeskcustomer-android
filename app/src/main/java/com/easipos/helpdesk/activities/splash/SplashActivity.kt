package com.easipos.helpdesk.activities.splash

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.splash.mvp.SplashPresenter
import com.easipos.helpdesk.activities.splash.mvp.SplashView
import com.easipos.helpdesk.activities.splash.navigation.SplashNavigation
import com.easipos.helpdesk.base.CustomBaseAppCompatActivity
import com.easipos.helpdesk.bundle.ParcelData
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.fragments.dialog_fragment.SplashClientConfigDialogFragment
import com.easipos.helpdesk.room.RoomService
import com.easipos.helpdesk.tools.Preference
import io.github.anderscheow.library.kotlinExt.TAG
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.rate
import io.github.anderscheow.validator.Validator
import org.kodein.di.generic.instance
import kotlin.system.exitProcess

class SplashActivity : CustomBaseAppCompatActivity(), SplashView {

    companion object {
        fun newIntent(context: Context, clearDb: Boolean = false, getClientUrl: Boolean = false): Intent {
            return Intent(context, SplashActivity::class.java).apply {
                this.putExtra(ParcelData.CLEAR_DB, clearDb)
                this.putExtra(ParcelData.GET_CLIENT_URL, getClientUrl)
            }
        }
    }

    //region Variables
    private val navigation by instance<SplashNavigation>()
    private val roomService by instance<RoomService>()
    private val validator by instance<Validator>()

    private val presenter by lazy { SplashPresenter(application) }

    private val clearDb by argument(ParcelData.CLEAR_DB, false)
    private val getClientUrl by argument(ParcelData.GET_CLIENT_URL, true)
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_splash

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        if (clearDb) {
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.clearAllTables()
                }
            }
        }

        showClientNameDialog()
    }
    //endregion

    //region SplashView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }

    override fun showUpdateAppDialog() {
        showYesAlertDialog(getString(R.string.prompt_update_app), buttonText = R.string.action_upgrade_now) {
            this@SplashActivity.rate()
            finishAffinity()
        }
    }

    override fun navigateToLogin() {
        navigation.navigateToLogin(this)
    }

    override fun navigateToMain() {
        navigation.navigateToMain(this)
    }

    override fun showClientNameDialog() {
        if (Preference.prefClientName.isBlank()) {
            removeDialogFragmentThen(SplashClientConfigDialogFragment.TAG) {
                SplashClientConfigDialogFragment.newInstance()
            }
        } else {
            performDuringSplash()
        }
    }

    override fun exitApplication() {
        exitProcess(0)
    }
    //endregion

    //region Action Methods
    fun performDuringSplash() {
        if (getClientUrl) {
            presenter.getClientUrl()
        } else {
            presenter.checkIsAuthenticated()
        }
    }
    //endregion
}
