package com.easipos.helpdesk.activities.create_view_case.mvp

import com.easipos.helpdesk.base.View
import com.easipos.helpdesk.models.CaseSubType
import com.easipos.helpdesk.models.CaseType
import com.easipos.helpdesk.models.CustomerOutlet
import com.easipos.helpdesk.models.OutletPos

interface CreateViewCaseView : View {

    fun showCustomerOutletSelections(customerOutlets: List<CustomerOutlet>)

    fun showOutletPosSelections(outletPosList: List<OutletPos>)

    fun autoPopulateFirstOutletPos(outletPos: OutletPos)

    fun showCaseTypeSelection(caseTypes: List<CaseType>)

    fun showCaseSubType1Selection(caseSubTypes: List<CaseSubType>)

    fun finishScreen()
}
