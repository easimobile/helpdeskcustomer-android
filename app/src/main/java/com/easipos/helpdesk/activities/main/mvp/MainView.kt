package com.easipos.helpdesk.activities.main.mvp

import androidx.fragment.app.DialogFragment
import com.easipos.helpdesk.base.View

interface MainView : View {

    fun navigateToSplash()

    fun markCaseAsRated(dialogFragment: DialogFragment, caseId: String)
}
