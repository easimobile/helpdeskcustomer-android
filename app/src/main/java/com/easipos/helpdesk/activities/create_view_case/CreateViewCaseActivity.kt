package com.easipos.helpdesk.activities.create_view_case

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.lifecycle.ViewModelProvider
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.create_view_case.mvp.CreateViewCasePresenter
import com.easipos.helpdesk.activities.create_view_case.mvp.CreateViewCaseView
import com.easipos.helpdesk.activities.create_view_case.navigation.CreateViewCaseNavigation
import com.easipos.helpdesk.activities.create_view_case.viewmodel.CreateViewCaseViewModel
import com.easipos.helpdesk.activities.create_view_case.viewmodel.CreateViewCaseViewModelFactory
import com.easipos.helpdesk.adapters.SelectionAdapter
import com.easipos.helpdesk.api.requests.cases.*
import com.easipos.helpdesk.base.CustomLifecycleActivity
import com.easipos.helpdesk.bundle.ParcelData
import com.easipos.helpdesk.event_bus.RefreshCase
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.models.*
import com.easipos.helpdesk.tools.Preference
import com.easipos.helpdesk.util.showBottomSheet
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.stfalcon.imageviewer.StfalconImageViewer
import io.github.anderscheow.library.constant.EventBusType
import io.github.anderscheow.library.kotlinExt.*
import io.github.anderscheow.library.viewModel.util.AlertDialogData
import kotlinx.android.synthetic.main.activity_create_view_case.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance
import java.io.ByteArrayOutputStream

class CreateViewCaseActivity : CustomLifecycleActivity<CreateViewCaseViewModel>(), CreateViewCaseView,
    SelectionAdapter.OnGestureDetectedListener {

    companion object {
        // caseId for View Case
        fun newIntent(context: Context, caseId: String?): Intent {
            return Intent(context, CreateViewCaseActivity::class.java).apply {
                this.putExtra(ParcelData.CASE_ID, caseId)
            }
        }
    }

    //region Variables
    private val presenter by lazy { CreateViewCasePresenter(application) }
    private val factory by lazy { CreateViewCaseViewModelFactory(application) }

    private val navigation by instance<CreateViewCaseNavigation>()

    private val caseId by argument<String>(ParcelData.CASE_ID)

    private var caseInfo: CaseInfo? = null

    private var selectedCaseType: CaseType? = null
    private var selectedCaseSubType1: CaseSubType? = null
    private var selectedOutlet: CustomerOutlet? = null
    private var selectedPos: OutletPos? = null
    private var selectedScreenshot: Bitmap? = null

    private var selectionDialog: BottomSheetDialog? = null
    private var isCaseSubType1Required = false
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            try {
                ImagePicker.getFilePath(data)?.let { filePath ->
                    selectedScreenshot = BitmapFactory.decodeFile(filePath)
                    setImageToScreenshot()
                }
            } catch (ex: Exception) {
                toastMessage(ex.localizedMessage?.toString() ?: "")
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            toastMessage(ImagePicker.getError(data))
        }
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_create_view_case

    override fun getEventBusType(): EventBusType? = EventBusType.ON_CREATE

    override fun setupViewModel(): CreateViewCaseViewModel {
        return ViewModelProvider(this, factory)
            .get(CreateViewCaseViewModel::class.java)
    }

    override fun setupViewModelObserver() {
        viewModel.caseInfoLD.observe(this) { caseInfo ->
            this.caseInfo = caseInfo

            invalidateClickListener()

            edit_text_contact_person.disable()
            edit_text_contact_no.disable()
            edit_text_remark.disable()
            fab.hide()
        }
    }

    override fun showAlertDialog(data: AlertDialogData) {
        showErrorAlertDialog(data.message, null, data.action ?: {})
    }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun showCustomerOutletSelections(customerOutlets: List<CustomerOutlet>) {
        val selectableList = customerOutlets.map { Selectable(it as Any) }
        selectedOutlet?.let { customerOutlet ->
            val selectable = selectableList.find {
                (it.data as CustomerOutlet).outletCode == customerOutlet.outletCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_outlet), selectableList)
    }

    override fun showOutletPosSelections(outletPosList: List<OutletPos>) {
        val selectableList = outletPosList.map { Selectable(it as Any) }
        selectedPos?.let { outletPos ->
            val selectable = selectableList.find {
                (it.data as OutletPos).posNo == outletPos.posNo
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_pos), selectableList)
    }

    override fun autoPopulateFirstOutletPos(outletPos: OutletPos) {
        onSelectItem(-1, outletPos)
    }

    override fun showCaseTypeSelection(caseTypes: List<CaseType>) {
        val selectableList = caseTypes.map { Selectable(it as Any) }
        selectedCaseType?.let { caseType ->
            val selectable = selectableList.find {
                (it.data as CaseType).caseTypeCode == caseType.caseTypeCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_case_type), selectableList)
    }

    override fun showCaseSubType1Selection(caseSubTypes: List<CaseSubType>) {
        val selectableList = caseSubTypes.map { Selectable(CaseSubTypeLayering(it, 1) as Any) }
        selectedCaseSubType1?.let { caseSubType ->
            val selectable = selectableList.find {
                (it.data as CaseSubTypeLayering).caseSubType.caseSubTypeCode == caseSubType.caseSubTypeCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_case_sub_type_1), selectableList)
    }

    override fun finishScreen() {
        finish()
    }
    //endregion

    //region Interface Methods
    override fun onSelectItem(position: Int, data: Any) {
        selectionDialog?.dismiss()

        when (data) {
            is CustomerOutlet -> {
                selectedOutlet = data
                selectedPos = null

                text_view_outlet.text = data.toString()
                text_view_pos.text = null

                card_view_pos.visible()

                if (data.posCount == 1) {
                    getOutletPosList(true)
                }
            }

            is OutletPos -> {
                selectedPos = data

                text_view_pos.text = data.toString()
            }

            is CaseType -> {
                selectedCaseType = data
                selectedCaseSubType1 = null

                text_view_case_type.text = data.toString()
                text_view_case_sub_type_1.text = null

                if (data.hasCaseSubType) {
                    isCaseSubType1Required = true
                    card_view_case_sub_type_1.visible()
                } else {
                    isCaseSubType1Required = false
                    card_view_case_sub_type_1.invisible()
                }
            }

            is CaseSubTypeLayering -> {
                when (data.level) {
                    1 -> { // CaseSubType1
                        selectedCaseSubType1 = data.caseSubType

                        text_view_case_sub_type_1.text = data.caseSubType.toString()
                    }
                }
            }
        }
    }

    override fun onReturnItems(items: List<Any>) {
    }
    //endregion

    //region Action Methods
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: Any) {
        if (event is RefreshCase) {
            loadCase()
        }
    }

    private fun setupViews() {
        card_view_case_sub_type_2.gone()
        card_view_case_sub_type_3.gone()
        card_view_sla.gone()
        layout_screenshot.gone()
        
        if (caseId == null) {
            text_view_title.setText(R.string.title_create_new_case)
            Preference.prefCustomerInfo?.let { customerInfo ->
                edit_text_contact_person.post {
                    edit_text_contact_person.setText(customerInfo.contactPerson)
                }
                edit_text_contact_no.post {
                    edit_text_contact_no.setText(customerInfo.contactNo)
                }
            }
        }

        loadCase()
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        fab.click {
            attemptCreateEditCase()
        }

        invalidateClickListener()
    }

    private fun invalidateClickListener() {
        if (caseInfo == null) {
            card_view_case_type.click {
                getCaseTypes()
            }

            card_view_case_sub_type_1.click {
                getCaseSubTypes1()
            }

            card_view_outlet.click {
                getCustomerOutlets()
            }

            card_view_pos.click {
                getOutletPosList(false)
            }

            image_view_add_screenshot.click {
                showImagePicker()
            }

            image_view_screenshot.click {
                showImageViewer()
            }
        } else {
            card_view_case_type.setOnClickListener(null)
            card_view_case_sub_type_1.setOnClickListener(null)
            card_view_outlet.setOnClickListener(null)
            card_view_pos.setOnClickListener(null)
            image_view_add_screenshot.setOnClickListener(null)
            image_view_screenshot.setOnClickListener(null)
        }
    }

    private fun loadCase() {
        caseId?.let { caseId ->
            val model = GetCaseInfoRequestModel(
                caseId = caseId
            )

            viewModel.getCaseInfo(this, model)
        }
    }

    private fun getCaseTypes() {
        presenter.doGetCaseTypes()
    }

    private fun getCaseSubTypes1() {
        selectedCaseType?.let {
            val model = GetCaseSubTypesRequestModel(
                code = it.caseTypeCode
            )

            presenter.doGetCaseSubTypes1(model)
        }
    }

    private fun getCustomerOutlets() {
        val model = GetCustomerOutletsRequestModel(
            projectCode = Preference.prefProjectCode
        )

        presenter.doGetCustomerOutlets(model)
    }

    private fun getOutletPosList(autoPopulate: Boolean) {
        selectedOutlet?.let {
            val model = GetOutletPosListRequestModel(
                outletCode = it.outletCode
            )

            presenter.doGetOutletPosList(model, autoPopulate)
        }
    }

    private fun showBottomSheetSelection(title: String, data: List<Selectable<Any>>) {
        selectionDialog?.dismiss()
        selectionDialog = showBottomSheet(this, this, title, data)
        selectionDialog?.show()
    }

    private fun showImagePicker() {
        ImagePicker.with(this)
            .compress(1024)
            .start()
    }

    private fun showImageViewer() {
        selectedScreenshot?.let { screenshot ->
            StfalconImageViewer.Builder(this, arrayOf(screenshot)) { view, image ->
                view.setImageBitmap(image)
            }.withTransitionFrom(image_view_screenshot)
                .withHiddenStatusBar(false)
                .show()
        }
    }

    private fun setImageToScreenshot() {
        selectedScreenshot?.let { screenshot ->
            image_view_screenshot.setImageBitmap(screenshot)
            layout_screenshot.visible()
        } ?: run {
            image_view_screenshot.setImageBitmap(null)
            layout_screenshot.gone()
        }
    }

    private fun attemptCreateEditCase() {
        if (selectedOutlet == null) {
            showErrorAlertDialog(getString(R.string.error_outlet_required))
            return
        }

        if (selectedPos == null) {
            showErrorAlertDialog(getString(R.string.error_pos_required))
            return
        }

        if (selectedCaseType == null) {
            showErrorAlertDialog(getString(R.string.error_case_type_required))
            return
        }

        if (isCaseSubType1Required && selectedCaseSubType1 == null) {
            showErrorAlertDialog(getString(R.string.error_case_sub_type_1_required))
            return
        }

        // Remove mandatory checking on case sub type 2 and 3
//        if (isCaseSubType2Required && selectedCaseSubType2 == null) {
//            showErrorAlertDialog(getString(R.string.error_case_sub_type_2_required))
//            return
//        }
//
//        if (isCaseSubType3Required && selectedCaseSubType3 == null) {
//            showErrorAlertDialog(getString(R.string.error_case_sub_type_3_required))
//            return
//        }


        if (caseId == null) {
            createCase()
        }
    }

    private fun createCase() {
        val model = CreateCaseRequestModel(
            outletCode = selectedOutlet!!.outletCode,
            posNo = selectedPos!!.posNo,
            contactPerson = edit_text_contact_person.text?.toString(),
            contactNo = edit_text_contact_no.text?.toString(),
            remarks = edit_text_remark.text?.toString(),
            caseTypeCode = selectedCaseType!!.caseTypeCode,
            caseSubTypeCode1 = selectedCaseSubType1?.caseSubTypeCode,
            caseSubTypeCode2 = null,
            caseSubTypeCode3 = null,
            screenshot = encodeImageToBase64()
        )

        presenter.doCreateCase(model)
    }

    private fun encodeImageToBase64(): String? {
        selectedScreenshot?.let { screenshot ->
            val outputStream = ByteArrayOutputStream()
            screenshot.compress(Bitmap.CompressFormat.JPEG, 75, outputStream)
            val byteArray = outputStream.toByteArray()
            return Base64.encodeToString(byteArray, Base64.DEFAULT)
        }

        return null
    }
    //endregion
}
