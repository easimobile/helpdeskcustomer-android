package com.easipos.helpdesk.activities.main.navigation

import android.app.Activity

interface MainNavigation {

    fun navigateToSplash(activity: Activity)

    fun navigateToCreateCase(activity: Activity)

    fun navigateToViewCase(activity: Activity, caseId: String)

    fun navigateToChangePassword(activity: Activity)
}