package com.easipos.helpdesk.activities.create_view_case.mvp

import android.app.Application
import com.easipos.helpdesk.R
import com.easipos.helpdesk.api.requests.cases.CreateCaseRequestModel
import com.easipos.helpdesk.api.requests.cases.GetCaseSubTypesRequestModel
import com.easipos.helpdesk.api.requests.cases.GetCustomerOutletsRequestModel
import com.easipos.helpdesk.api.requests.cases.GetOutletPosListRequestModel
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.event_bus.RefreshCaseListing
import com.easipos.helpdesk.models.*
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.kodein.di.generic.instance

class CreateViewCasePresenter(application: Application)
    : Presenter<CreateViewCaseView>(application) {

    private val caseRepository by instance<CaseRepository>()

    fun doGetCustomerOutlets(model: GetCustomerOutletsRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCustomerOutlets(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<CustomerOutlet>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showCustomerOutletSelections(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetOutletPosList(model: GetOutletPosListRequestModel, autoPopulate: Boolean) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getOutletPosList(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<OutletPos>> -> {
                        view?.setLoadingIndicator(false)
                        if (autoPopulate) {
                            if (result.data.isNotEmpty()) {
                                view?.autoPopulateFirstOutletPos(result.data[0])
                            } else {
                                // Workaround to fix if else expression compile error
                                Unit
                            }
                        } else {
                            view?.showOutletPosSelections(result.data)
                        }
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetCaseTypes() {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCaseTypes()
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<CaseType>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showCaseTypeSelection(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetCaseSubTypes1(model: GetCaseSubTypesRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCaseSubTypes1(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<CaseSubType>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showCaseSubType1Selection(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doCreateCase(model: CreateCaseRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.createCase(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<String> -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(application.getString(R.string.prompt_case_create_successful)) {
                            view?.finishScreen()
                        }

                        EventBus.getDefault().post(RefreshCaseListing())
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }
}
