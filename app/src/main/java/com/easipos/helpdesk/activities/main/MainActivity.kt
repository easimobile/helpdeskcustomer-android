package com.easipos.helpdesk.activities.main

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.main.mvp.MainPresenter
import com.easipos.helpdesk.activities.main.mvp.MainView
import com.easipos.helpdesk.activities.main.navigation.MainNavigation
import com.easipos.helpdesk.api.requests.cases.SubmitRatingRequestModel
import com.easipos.helpdesk.base.CustomBaseAppCompatActivity
import com.easipos.helpdesk.constant.NotificationCategory
import com.easipos.helpdesk.fragments.account.AccountFragment
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.fragments.cases.CaseFragment
import com.easipos.helpdesk.managers.FcmManager
import com.easipos.helpdesk.managers.PushNotificationManager
import com.google.android.material.textview.MaterialTextView
import com.onesignal.OneSignal
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.findColor
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.textColor
import org.kodein.di.generic.instance

class MainActivity : CustomBaseAppCompatActivity(), MainView {

    companion object {
        const val HOME_TAB = 0 // Aka Case
        const val ACCOUNT_TAB = 1

        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    enum class MainNavTab {
        HOME, // Aka Case
        OTHER_1,
        OTHER_2,
        ACCOUNT
    }

    //region Variables
    private val navigation by instance<MainNavigation>()
    private val fcmManager by instance<FcmManager>()
    private val pushNotificationManager by instance<PushNotificationManager>()

    private val presenter by lazy { MainPresenter(application) }

    private val caseFragment by lazy { CaseFragment.newInstance() }
    private val accountFragment by lazy { AccountFragment.newInstance() }

    private var selectedTab = MainNavTab.HOME
    private val tabFragments = arrayListOf<Fragment>()
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        observeOneSignalSubscription()
        registerPushTokenIfPossible()
        processPayload()

        setupViews()
        setupListeners()

        selectTab()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }

    override fun markCaseAsRated(dialogFragment: DialogFragment, caseId: String) {
        dialogFragment.dismissAllowingStateLoss()
        caseFragment.getCases()
    }

    override fun navigateToSplash() {
        navigation.navigateToSplash(this)
    }
    //endregion

    //region Action Methods
    fun processPayload() {
        Logger.d(pushNotificationManager.payload)
        pushNotificationManager.payload?.let { payload ->
            payload.category?.let { category ->
                NotificationCategory.parse(category)?.let { notificationCategory ->
                    when (notificationCategory) {
                        NotificationCategory.CASE_ASSIGNED_TO_AGENT,
                        NotificationCategory.CASE_CLOSED -> {
                            payload.arg?.let { caseId ->
                                navigation.navigateToViewCase(this@MainActivity, caseId)
                            }
                        }
                    }
                }
            }
            pushNotificationManager.removePayload()
        }
    }

    fun doLogout() {
        presenter.doLogout()
    }

    fun refreshCases() {
        caseFragment.getCases()
    }

    fun submitRating(dialogFragment: DialogFragment, model: SubmitRatingRequestModel) {
        presenter.doSubmitRating(dialogFragment, model)
    }

    fun navigateToChangePassword() {
        navigation.navigateToChangePassword(this)
    }

    fun navigateToViewCase(caseId: String) {
        navigation.navigateToViewCase(this, caseId)
    }

    private fun observeOneSignalSubscription() {
        OneSignal.addSubscriptionObserver { state ->
            Logger.d(state.toString())
            fcmManager.service.saveFcmToken(state.to.userId)
        }
    }

    private fun registerPushTokenIfPossible() {
        fcmManager.service.registerFcmToken()
    }

    private fun setupViews() {
        tabFragments.apply {
            this.add(caseFragment)
            this.add(accountFragment)
        }

        view_pager.apply {
            this.adapter = MainViewPagerAdapter(this@MainActivity)
            this.offscreenPageLimit = 10
            this.isUserInputEnabled = false
        }
    }

    private fun setupListeners() {
        fab.click {
            navigation.navigateToCreateCase(this)
        }

        text_view_nav_home.click {
            if (selectedTab != MainNavTab.HOME) {
                selectedTab = MainNavTab.HOME

                selectTab()
            }
        }

        text_view_nav_other_1.click {
            if (selectedTab != MainNavTab.OTHER_1) {
                selectedTab = MainNavTab.OTHER_1

                selectTab()
            }
        }

        text_view_nav_other_2.click {
            if (selectedTab != MainNavTab.OTHER_2) {
                selectedTab = MainNavTab.OTHER_2

                selectTab()
            }
        }

        text_view_nav_account.click {
            if (selectedTab != MainNavTab.ACCOUNT) {
                selectedTab = MainNavTab.ACCOUNT

                selectTab()
            }
        }
    }

    private fun selectTab() {
        when (selectedTab) {
            MainNavTab.HOME -> {
                setActiveTab(text_view_nav_home)

                setInactiveTab(text_view_nav_other_1)
                setInactiveTab(text_view_nav_other_2)
                setInactiveTab(text_view_nav_account)

                view_pager.setCurrentItem(HOME_TAB, false)
            }

            MainNavTab.OTHER_1 -> {
                setActiveTab(text_view_nav_other_1)

                setInactiveTab(text_view_nav_home)
                setInactiveTab(text_view_nav_other_2)
                setInactiveTab(text_view_nav_account)
            }

            MainNavTab.OTHER_2 -> {
                setActiveTab(text_view_nav_other_2)

                setInactiveTab(text_view_nav_home)
                setInactiveTab(text_view_nav_other_1)
                setInactiveTab(text_view_nav_account)
            }

            MainNavTab.ACCOUNT -> {
                setActiveTab(text_view_nav_account)

                setInactiveTab(text_view_nav_home)
                setInactiveTab(text_view_nav_other_1)
                setInactiveTab(text_view_nav_other_2)

                view_pager.setCurrentItem(ACCOUNT_TAB, false)
            }
        }
    }

    private fun setActiveTab(textView: MaterialTextView) {
        val tabActiveColor = findColor(R.color.colorTabActive)

        textView.textColor = tabActiveColor
        textView.compoundDrawables.getOrNull(1)?.mutate()?.let { drawable ->
            drawable.colorFilter = PorterDuffColorFilter(tabActiveColor, PorterDuff.Mode.SRC_IN)
        }
    }

    private fun setInactiveTab(textView: MaterialTextView) {
        val tabInactiveColor = findColor(R.color.colorTabInactive)

        textView.textColor = tabInactiveColor
        textView.compoundDrawables.getOrNull(1)?.mutate()?.let { drawable ->
            drawable.colorFilter = PorterDuffColorFilter(tabInactiveColor, PorterDuff.Mode.SRC_IN)
        }
    }
    //endregion

    inner class MainViewPagerAdapter(fragmentActivity: FragmentActivity)
        : FragmentStateAdapter(fragmentActivity) {

        override fun getItemCount(): Int = tabFragments.size

        override fun createFragment(position: Int): Fragment {
            return tabFragments[position]
        }
    }
}
