package com.easipos.helpdesk.activities.main.navigation

import android.app.Activity
import com.easipos.helpdesk.activities.change_password.ChangePasswordActivity
import com.easipos.helpdesk.activities.create_view_case.CreateViewCaseActivity
import com.easipos.helpdesk.activities.splash.SplashActivity

class MainNavigationImpl : MainNavigation {

    override fun navigateToSplash(activity: Activity) {
        activity.startActivity(SplashActivity.newIntent(activity))
        activity.finish()
    }

    override fun navigateToCreateCase(activity: Activity) {
        activity.startActivity(CreateViewCaseActivity.newIntent(activity, null))
    }

    override fun navigateToViewCase(activity: Activity, caseId: String) {
        activity.startActivity(CreateViewCaseActivity.newIntent(activity, caseId))
    }

    override fun navigateToChangePassword(activity: Activity) {
        activity.startActivity(ChangePasswordActivity.newIntent(activity))
    }
}