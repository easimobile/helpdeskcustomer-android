package com.easipos.helpdesk.activities.splash.mvp

import com.easipos.helpdesk.base.View

interface SplashView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()

    fun showClientNameDialog()

    fun exitApplication()
}
