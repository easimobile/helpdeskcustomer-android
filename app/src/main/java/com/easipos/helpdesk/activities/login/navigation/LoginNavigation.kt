package com.easipos.helpdesk.activities.login.navigation

import android.app.Activity

interface LoginNavigation {

    fun navigateToResetPassword(activity: Activity)

    fun navigateToChangePassword(activity: Activity)

    fun navigateToMain(activity: Activity)
}