package com.easipos.helpdesk.activities.create_view_case.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CreateViewCaseViewModelFactory(private val application: Application)
    : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CreateViewCaseViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CreateViewCaseViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}