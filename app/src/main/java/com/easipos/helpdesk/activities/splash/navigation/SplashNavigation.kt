package com.easipos.helpdesk.activities.splash.navigation

import android.app.Activity

interface SplashNavigation {

    fun navigateToLogin(activity: Activity)

    fun navigateToMain(activity: Activity)
}