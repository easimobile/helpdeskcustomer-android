package com.easipos.helpdesk.bundle

object ParcelData {

    const val CLEAR_DB = "clear_db"
    const val URL = "url"
    const val CASE = "CASE"
    const val CASE_ID = "CASE_ID"
    const val GET_CLIENT_URL = "GET_CLIENT_URL"
}