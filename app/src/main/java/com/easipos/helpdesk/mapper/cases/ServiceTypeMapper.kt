package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.ServiceTypeResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.ServiceType

class ServiceTypeMapper : Mapper<ServiceType, ServiceTypeResponseModel>() {

    override fun transform(item: ServiceTypeResponseModel): ServiceType {
        return ServiceType(
            serviceTypeCode = item.serviceTypeCode ?: "",
            serviceTypeDesc = item.serviceTypeDesc ?: ""
        )
    }
}