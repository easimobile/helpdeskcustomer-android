package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.CaseTypeResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.CaseType

class CaseTypeMapper : Mapper<CaseType, CaseTypeResponseModel>() {

    override fun transform(item: CaseTypeResponseModel): CaseType {
        return CaseType(
            caseTypeCode = item.caseTypeCode ?: "",
            caseTypeDesc = item.caseTypeDesc ?: "",
            hasCaseSubType = item.hasCaseSubType == "1"
        )
    }
}