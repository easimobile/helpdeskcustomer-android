package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.OutletPosResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.OutletPos

class OutletPosMapper : Mapper<OutletPos, OutletPosResponseModel>() {

    override fun transform(item: OutletPosResponseModel): OutletPos {
        return OutletPos(
            posNo = item.posNo ?: "",
            posDesc = item.posDesc ?: "",
            ipAddress = item.ipAddress ?: ""
        )
    }
}