package com.easipos.helpdesk.mapper.user

import com.easipos.helpdesk.api.responses.user.CustomerInfoResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.CustomerInfo

class CustomerInfoMapper : Mapper<CustomerInfo, CustomerInfoResponseModel>() {

    override fun transform(item: CustomerInfoResponseModel): CustomerInfo {
        return CustomerInfo(
            customerName = item.customerName ?: "",
            projectName = item.projectName ?: "",
            contactPerson = item.contactPerson ?: "",
            contactNo = item.contactNo ?: ""
        )
    }
}