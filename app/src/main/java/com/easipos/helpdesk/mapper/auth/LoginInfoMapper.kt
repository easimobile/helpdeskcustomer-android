package com.easipos.helpdesk.mapper.auth

import com.easipos.helpdesk.api.responses.auth.LoginInfoResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.LoginInfo

class LoginInfoMapper : Mapper<LoginInfo, LoginInfoResponseModel>() {

    override fun transform(item: LoginInfoResponseModel): LoginInfo {
        return LoginInfo(
            apiKey = item.apiKey ?: "",
            projectCode = item.projectCode ?: ""
        )
    }
}