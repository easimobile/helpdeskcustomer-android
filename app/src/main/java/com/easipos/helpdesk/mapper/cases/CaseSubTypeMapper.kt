package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.CaseSubTypeResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.CaseSubType

class CaseSubTypeMapper : Mapper<CaseSubType, CaseSubTypeResponseModel>() {

    override fun transform(item: CaseSubTypeResponseModel): CaseSubType {
        return CaseSubType(
            caseSubTypeCode = item.caseSubTypeCode ?: "",
            caseSubTypeDesc = item.caseSubTypeDesc ?: "",
            hasCaseSubType = item.hasCaseSubType == "1"
        )
    }
}