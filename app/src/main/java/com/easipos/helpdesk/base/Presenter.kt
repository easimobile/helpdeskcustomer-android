package com.easipos.helpdesk.base

import android.app.Application
import androidx.annotation.UiThread
import com.easipos.helpdesk.Easi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.kodein.di.KodeinAware
import java.lang.ref.WeakReference
import kotlin.coroutines.CoroutineContext

interface MvpPresenter<V : View> {
    @UiThread
    fun onAttachView(view: V)

    @UiThread
    fun onDetachView()
}

open class Presenter<V : View>(val application: Application) : MvpPresenter<V>,
    KodeinAware, CoroutineScope {

    private val job = Job()

    override val kodein by (application as Easi).kodein
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    private var viewRef: WeakReference<V>? = null

    var view: V? = null
        private set
        get() = viewRef?.get()

    @UiThread
    override fun onAttachView(view: V) {
        viewRef = WeakReference(view)
    }

    @UiThread
    override fun onDetachView() {
        if (viewRef != null) {
            viewRef!!.clear()
            viewRef = null
        }
        job.cancel()
    }

    @UiThread
    fun isViewAttached(): Boolean {
        return viewRef != null && viewRef!!.get() != null
    }
}