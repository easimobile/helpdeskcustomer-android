package com.easipos.helpdesk.datasource.precheck

import com.easipos.helpdesk.api.misc.parseException
import com.easipos.helpdesk.api.misc.parseResponse
import com.easipos.helpdesk.api.requests.precheck.CheckVersionRequestModel
import com.easipos.helpdesk.api.requests.precheck.GetClientUrlRequestModel
import com.easipos.helpdesk.api.services.Api
import com.easipos.helpdesk.api.services.GlobalApi
import com.easipos.helpdesk.models.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PrecheckDataSource(private val globalApi: GlobalApi,
                         private val api: Api) : PrecheckDataStore {

    override suspend fun getClientUrl(model: GetClientUrlRequestModel): Result<String> {
        return withContext(Dispatchers.IO) {
            try {
                val response = globalApi.getClientUrl(model.clientName)
                parseResponse(response) {
                    it
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.checkVersion(model.toFormDataBuilder().build())
                parseResponse(response) {
                    it
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }
}
