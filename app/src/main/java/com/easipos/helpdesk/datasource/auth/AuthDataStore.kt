package com.easipos.helpdesk.datasource.auth

import com.easipos.helpdesk.api.requests.auth.LoginRequestModel
import com.easipos.helpdesk.mapper.auth.LoginInfoMapper
import com.easipos.helpdesk.models.LoginInfo
import com.easipos.helpdesk.models.ResponseWrapper
import com.easipos.helpdesk.models.Result

interface AuthDataStore {

    suspend fun login(model: LoginRequestModel, loginInfoMapper: LoginInfoMapper): Result<ResponseWrapper<LoginInfo>>
}
