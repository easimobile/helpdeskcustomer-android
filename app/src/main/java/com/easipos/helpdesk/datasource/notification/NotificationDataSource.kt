package com.easipos.helpdesk.datasource.notification

import com.easipos.helpdesk.api.misc.parseException
import com.easipos.helpdesk.api.misc.parseResponse
import com.easipos.helpdesk.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.helpdesk.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.helpdesk.api.services.Api
import com.easipos.helpdesk.models.Result

class NotificationDataSource(private val api: Api) : NotificationDataStore {

    override suspend fun registerFcmToken(model: RegisterFcmTokenRequestModel): Result<Nothing> {
        return try {
            val response = api.registerFcmToken(model)
            parseResponse(response)
        } catch (ex: Exception) {
            parseException(ex)
        }
    }

    override suspend fun removeFcmToken(model: RemoveFcmTokenRequestModel): Result<Nothing> {
        return try {
            val response = api.removeFcmToken(model)
            parseResponse(response)
        } catch (ex: Exception) {
            parseException(ex)
        }
    }
}
