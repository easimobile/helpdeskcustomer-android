package com.easipos.helpdesk.datasource.cases

import com.easipos.helpdesk.api.requests.cases.*
import com.easipos.helpdesk.mapper.cases.*
import com.easipos.helpdesk.models.*

interface CaseDataStore {

    suspend fun getCases(model: GetCasesRequestModel, caseMapper: CaseMapper): Result<DataSourceListModel<Case>>

    suspend fun getCaseInfo(model: GetCaseInfoRequestModel, caseInfoMapper: CaseInfoMapper): Result<CaseInfo>

    suspend fun getCustomerOutlets(model: GetCustomerOutletsRequestModel, customerOutletMapper: CustomerOutletMapper): Result<List<CustomerOutlet>>

    suspend fun getOutletPosList(model: GetOutletPosListRequestModel, outletPosMapper: OutletPosMapper): Result<List<OutletPos>>

    suspend fun getServiceTypes(serviceTypeMapper: ServiceTypeMapper): Result<List<ServiceType>>

    suspend fun getCaseTypes(caseTypeMapper: CaseTypeMapper): Result<List<CaseType>>

    suspend fun getCaseSubTypes1(model: GetCaseSubTypesRequestModel, caseSubTypeMapper: CaseSubTypeMapper): Result<List<CaseSubType>>

    suspend fun createCase(model: CreateCaseRequestModel): Result<String>

    suspend fun submitRating(model: SubmitRatingRequestModel): Result<Nothing>
}
