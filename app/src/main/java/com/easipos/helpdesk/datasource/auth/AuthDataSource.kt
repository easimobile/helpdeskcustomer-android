package com.easipos.helpdesk.datasource.auth

import com.easipos.helpdesk.api.misc.parseException
import com.easipos.helpdesk.api.misc.parseResponse
import com.easipos.helpdesk.api.requests.auth.LoginRequestModel
import com.easipos.helpdesk.api.services.Api
import com.easipos.helpdesk.mapper.auth.LoginInfoMapper
import com.easipos.helpdesk.models.LoginInfo
import com.easipos.helpdesk.models.ResponseWrapper
import com.easipos.helpdesk.models.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AuthDataSource(private val api: Api) : AuthDataStore {

    override suspend fun login(model: LoginRequestModel, loginInfoMapper: LoginInfoMapper): Result<ResponseWrapper<LoginInfo>> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.login(model)
                parseResponse(response) {
                    ResponseWrapper(
                        code = response.code,
                        data = loginInfoMapper.transform(it)
                    )
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }
}
