package com.easipos.helpdesk.datasource.user

import com.easipos.helpdesk.api.requests.user.ChangePasswordRequestModel
import com.easipos.helpdesk.api.requests.user.ForgotPasswordRequestModel
import com.easipos.helpdesk.mapper.user.CustomerInfoMapper
import com.easipos.helpdesk.models.CustomerInfo
import com.easipos.helpdesk.models.Result

interface UserDataStore {

    suspend fun forgotPassword(model: ForgotPasswordRequestModel): Result<Nothing>

    suspend fun changePassword(model: ChangePasswordRequestModel): Result<Nothing>

    suspend fun getCustomerInfo(customerInfoMapper: CustomerInfoMapper): Result<CustomerInfo>
}
