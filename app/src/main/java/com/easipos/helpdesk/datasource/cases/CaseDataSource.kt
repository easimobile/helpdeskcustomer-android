package com.easipos.helpdesk.datasource.cases

import com.easipos.helpdesk.api.misc.parseException
import com.easipos.helpdesk.api.misc.parseResponse
import com.easipos.helpdesk.api.requests.BasicRequestModel
import com.easipos.helpdesk.api.requests.cases.*
import com.easipos.helpdesk.api.services.Api
import com.easipos.helpdesk.mapper.cases.*
import com.easipos.helpdesk.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CaseDataSource(private val api: Api) : CaseDataStore {

    override suspend fun getCases(model: GetCasesRequestModel, caseMapper: CaseMapper): Result<DataSourceListModel<Case>> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getCases(model)
                parseResponse(response) {
                    DataSourceListModel(
                        itemList = caseMapper.transform(it.list),
                        totalOfElements = it.total,
                        hasNext = it.hasNext
                    )
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun getCaseInfo(model: GetCaseInfoRequestModel, caseInfoMapper: CaseInfoMapper): Result<CaseInfo> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getCaseInfo(model.apiKey, model.caseId, model.isCustomer)
                parseResponse(response) {
                    caseInfoMapper.transform(it)
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun getCustomerOutlets(model: GetCustomerOutletsRequestModel, customerOutletMapper: CustomerOutletMapper): Result<List<CustomerOutlet>> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getCustomerOutlets(model.apiKey, model.projectCode)
                parseResponse(response) {
                    customerOutletMapper.transform(it)
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun getOutletPosList(model: GetOutletPosListRequestModel, outletPosMapper: OutletPosMapper): Result<List<OutletPos>> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getOutletPosList(model.apiKey, model.outletCode)
                parseResponse(response) {
                    outletPosMapper.transform(it)
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun getServiceTypes(serviceTypeMapper: ServiceTypeMapper): Result<List<ServiceType>> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getServiceTypes(BasicRequestModel().apiKey)
                parseResponse(response) {
                    serviceTypeMapper.transform(it)
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun getCaseTypes(caseTypeMapper: CaseTypeMapper): Result<List<CaseType>> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getCaseTypes(BasicRequestModel().apiKey)
                parseResponse(response) {
                    caseTypeMapper.transform(it)
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun getCaseSubTypes1(model: GetCaseSubTypesRequestModel, caseSubTypeMapper: CaseSubTypeMapper): Result<List<CaseSubType>> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getCaseSubTypes1(model.apiKey, model.code)
                parseResponse(response) {
                    caseSubTypeMapper.transform(it)
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun createCase(model: CreateCaseRequestModel): Result<String> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.createCase(model)
                parseResponse(response) {
                    it
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun submitRating(model: SubmitRatingRequestModel): Result<Nothing> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.submitRating(model)
                parseResponse(response)
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }
}
