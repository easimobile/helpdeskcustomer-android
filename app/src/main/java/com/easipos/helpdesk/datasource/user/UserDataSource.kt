package com.easipos.helpdesk.datasource.user

import com.easipos.helpdesk.api.misc.parseException
import com.easipos.helpdesk.api.misc.parseResponse
import com.easipos.helpdesk.api.requests.BasicRequestModel
import com.easipos.helpdesk.api.requests.user.ChangePasswordRequestModel
import com.easipos.helpdesk.api.requests.user.ForgotPasswordRequestModel
import com.easipos.helpdesk.api.services.Api
import com.easipos.helpdesk.mapper.user.CustomerInfoMapper
import com.easipos.helpdesk.models.CustomerInfo
import com.easipos.helpdesk.models.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserDataSource(private val api: Api) : UserDataStore {

    override suspend fun forgotPassword(model: ForgotPasswordRequestModel): Result<Nothing> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.forgotPassword(model)
                parseResponse(response)
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun changePassword(model: ChangePasswordRequestModel): Result<Nothing> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.changePassword(model)
                parseResponse(response)
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }

    override suspend fun getCustomerInfo(customerInfoMapper: CustomerInfoMapper): Result<CustomerInfo> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getUserInfo(BasicRequestModel().apiKey)
                parseResponse(response) {
                    customerInfoMapper.transform(it)
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }
}
