package com.easipos.helpdesk.constant

import java.util.*

enum class NotificationCategory {
    CASE_CLOSED,
    CASE_ASSIGNED_TO_AGENT;

    companion object {
        fun parse(value: String): NotificationCategory? {
            return when (value.toUpperCase(Locale.getDefault())) {
                "CASE_CLOSED" -> CASE_CLOSED
                "CASE_ASSIGNED_TO_AGENT" -> CASE_ASSIGNED_TO_AGENT
                else -> null
            }
        }
    }
}