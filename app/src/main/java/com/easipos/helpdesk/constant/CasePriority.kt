package com.easipos.helpdesk.constant

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
enum class CasePriority(val value: String) : Parcelable {
    LOW("Low"),
    MEDIUM("Medium"),
    HIGH("High"),
    CRITICAL("Critical");

    companion object {
        fun parse(value: String): CasePriority? {
            return when (value.toLowerCase(Locale.getDefault())) {
                "low" -> LOW
                "medium" -> MEDIUM
                "high" -> HIGH
                "critical" -> CRITICAL
                else -> null
            }
        }

        fun parse(value: Int): CasePriority? {
            return when (value) {
                4 -> LOW
                3 -> MEDIUM
                2 -> HIGH
                1 -> CRITICAL
                else -> null
            }
        }

        fun toList(): List<CasePriority> {
            return arrayListOf<CasePriority>().apply {
                this.add(CRITICAL)
                this.add(HIGH)
                this.add(MEDIUM)
                this.add(LOW)
            }
        }
    }

    override fun toString(): String {
        return this.value
    }

    fun toInt(): Int {
        return when (this) {
            LOW -> 4
            MEDIUM -> 3
            HIGH -> 2
            CRITICAL -> 1
        }
    }
}