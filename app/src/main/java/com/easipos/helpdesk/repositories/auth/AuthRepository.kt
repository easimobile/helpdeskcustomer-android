package com.easipos.helpdesk.repositories.auth

import com.easipos.helpdesk.api.requests.auth.LoginRequestModel
import com.easipos.helpdesk.models.LoginInfo
import com.easipos.helpdesk.models.ResponseWrapper
import com.easipos.helpdesk.models.Result

interface AuthRepository {

    suspend fun login(model: LoginRequestModel): Result<ResponseWrapper<LoginInfo>>
}
