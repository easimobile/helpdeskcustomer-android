package com.easipos.helpdesk.repositories.user

import com.easipos.helpdesk.api.requests.user.ChangePasswordRequestModel
import com.easipos.helpdesk.api.requests.user.ForgotPasswordRequestModel
import com.easipos.helpdesk.datasource.DataFactory
import com.easipos.helpdesk.mapper.user.CustomerInfoMapper
import com.easipos.helpdesk.models.CustomerInfo
import com.easipos.helpdesk.models.Result

class UserDataRepository(private val dataFactory: DataFactory) : UserRepository {

    private val userInfoMapper by lazy { CustomerInfoMapper() }

    override suspend fun forgotPassword(model: ForgotPasswordRequestModel): Result<Nothing> =
        dataFactory.createUserDataSource()
            .forgotPassword(model)

    override suspend fun changePassword(model: ChangePasswordRequestModel): Result<Nothing> =
        dataFactory.createUserDataSource()
            .changePassword(model)

    override suspend fun getCustomerInfo(): Result<CustomerInfo> =
        dataFactory.createUserDataSource()
            .getCustomerInfo(userInfoMapper)
}
