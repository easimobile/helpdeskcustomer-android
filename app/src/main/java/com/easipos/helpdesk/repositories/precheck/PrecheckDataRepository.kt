package com.easipos.helpdesk.repositories.precheck

import com.easipos.helpdesk.api.requests.precheck.CheckVersionRequestModel
import com.easipos.helpdesk.api.requests.precheck.GetClientUrlRequestModel
import com.easipos.helpdesk.datasource.DataFactory
import com.easipos.helpdesk.models.Result

class PrecheckDataRepository(private val dataFactory: DataFactory) : PrecheckRepository {

    override suspend fun getClientUrl(model: GetClientUrlRequestModel): Result<String> =
        dataFactory.createPrecheckDataSource()
            .getClientUrl(model)

    override suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
