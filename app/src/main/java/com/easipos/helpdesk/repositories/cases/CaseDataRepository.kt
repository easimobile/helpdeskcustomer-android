package com.easipos.helpdesk.repositories.cases

import com.easipos.helpdesk.api.requests.cases.*
import com.easipos.helpdesk.datasource.DataFactory
import com.easipos.helpdesk.mapper.cases.*
import com.easipos.helpdesk.models.*

class CaseDataRepository(private val dataFactory: DataFactory) : CaseRepository {

    private val caseMapper by lazy { CaseMapper() }
    private val caseInfoMapper by lazy { CaseInfoMapper() }
    private val customerOutletMapper by lazy { CustomerOutletMapper() }
    private val outletPosMapper by lazy { OutletPosMapper() }
    private val serviceTypeMapper by lazy { ServiceTypeMapper() }
    private val caseTypeMapper by lazy { CaseTypeMapper() }
    private val caseSubTypeMapper by lazy { CaseSubTypeMapper() }

    override suspend fun getCases(model: GetCasesRequestModel): Result<DataSourceListModel<Case>> =
        dataFactory.createCaseDataSource()
            .getCases(model, caseMapper)

    override suspend fun getCaseInfo(model: GetCaseInfoRequestModel): Result<CaseInfo> =
        dataFactory.createCaseDataSource()
            .getCaseInfo(model, caseInfoMapper)

    override suspend fun getCustomerOutlets(model: GetCustomerOutletsRequestModel): Result<List<CustomerOutlet>> =
        dataFactory.createCaseDataSource()
            .getCustomerOutlets(model, customerOutletMapper)

    override suspend fun getOutletPosList(model: GetOutletPosListRequestModel): Result<List<OutletPos>> =
        dataFactory.createCaseDataSource()
            .getOutletPosList(model, outletPosMapper)

    override suspend fun getServiceTypes(): Result<List<ServiceType>> =
        dataFactory.createCaseDataSource()
            .getServiceTypes(serviceTypeMapper)

    override suspend fun getCaseTypes(): Result<List<CaseType>> =
        dataFactory.createCaseDataSource()
            .getCaseTypes(caseTypeMapper)

    override suspend fun getCaseSubTypes1(model: GetCaseSubTypesRequestModel): Result<List<CaseSubType>> =
        dataFactory.createCaseDataSource()
            .getCaseSubTypes1(model, caseSubTypeMapper)

    override suspend fun createCase(model: CreateCaseRequestModel): Result<String> =
        dataFactory.createCaseDataSource()
            .createCase(model)

    override suspend fun submitRating(model: SubmitRatingRequestModel): Result<Nothing> =
        dataFactory.createCaseDataSource()
            .submitRating(model)
}
