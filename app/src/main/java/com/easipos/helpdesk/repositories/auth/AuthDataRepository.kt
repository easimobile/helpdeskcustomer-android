package com.easipos.helpdesk.repositories.auth

import com.easipos.helpdesk.api.requests.auth.LoginRequestModel
import com.easipos.helpdesk.datasource.DataFactory
import com.easipos.helpdesk.mapper.auth.LoginInfoMapper
import com.easipos.helpdesk.models.LoginInfo
import com.easipos.helpdesk.models.ResponseWrapper
import com.easipos.helpdesk.models.Result

class AuthDataRepository(private val dataFactory: DataFactory) : AuthRepository {

    private val loginInfoMapper by lazy { LoginInfoMapper() }

    override suspend fun login(model: LoginRequestModel): Result<ResponseWrapper<LoginInfo>> =
        dataFactory.createAuthDataSource()
            .login(model, loginInfoMapper)
}
