package com.easipos.helpdesk.repositories.cases

import com.easipos.helpdesk.api.requests.cases.*
import com.easipos.helpdesk.models.*

interface CaseRepository {

    suspend fun getCases(model: GetCasesRequestModel): Result<DataSourceListModel<Case>>

    suspend fun getCaseInfo(model: GetCaseInfoRequestModel): Result<CaseInfo>

    suspend fun getCustomerOutlets(model: GetCustomerOutletsRequestModel): Result<List<CustomerOutlet>>

    suspend fun getOutletPosList(model: GetOutletPosListRequestModel): Result<List<OutletPos>>

    suspend fun getServiceTypes(): Result<List<ServiceType>>

    suspend fun getCaseTypes(): Result<List<CaseType>>

    suspend fun getCaseSubTypes1(model: GetCaseSubTypesRequestModel): Result<List<CaseSubType>>

    suspend fun createCase(model: CreateCaseRequestModel): Result<String>

    suspend fun submitRating(model: SubmitRatingRequestModel): Result<Nothing>
}
