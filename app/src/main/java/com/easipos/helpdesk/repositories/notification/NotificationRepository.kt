package com.easipos.helpdesk.repositories.notification

import com.easipos.helpdesk.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.helpdesk.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.helpdesk.models.Result

interface NotificationRepository {

    suspend fun registerFcmToken(model: RegisterFcmTokenRequestModel): Result<Nothing>

    suspend fun removeFcmToken(model: RemoveFcmTokenRequestModel): Result<Nothing>
}
